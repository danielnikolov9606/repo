package com.telerikacademy.oop;

import java.util.Iterator;

public class MyListImpl<T> implements MyList<T> {
    private static final int CAPACITY = 4;
    private final T[] elements;
    private int size;


    public MyListImpl() {
        elements = (T[]) new Object[CAPACITY];
        size = 0;
    }
    public MyListImpl(int capacity) {
        elements = (T[]) new Object[capacity];
        size = 0;
    }


    @Override
    public int size() {
        return 0;
    }

    @Override
    public int capacity() {
        return 0;
    }

    @Override
    public T get(int index) {
        return elements[index];
    }

    @Override
    public void add(T element) {
        elements[size] = element;
        size++;
    }

    @Override
    public boolean contains(T element) {
        return false;
    }

    @Override
    public int indexOf(T element) {
        return 0;
    }

    @Override
    public int lastIndexOf(T element) {
        return 0;
    }

    @Override
    public boolean remove(T element) {
        return false;
    }

    @Override
    public boolean removeAt(int index) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public void swap(int from, int to) {

    }

    @Override
    public void print() {

    }

    @Override
    public Iterator<T> iterator() {
        return new MyArrayListIterator();
    }

    private class MyArrayListIterator implements Iterator<T> {
        int currentIndex;

        MyArrayListIterator() {
            currentIndex = 0;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < size;
        }

        @Override
        public T next() {
            T result = elements[currentIndex];
            currentIndex++;
            return result;
        }
    }

}
