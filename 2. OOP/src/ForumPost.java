import java.util.ArrayList;

public class ForumPost {
    String author;
    String text;
    int upVotes;
    ArrayList<String> replies;

    public ForumPost(String author, String text, int upVotes) {
        this.author = author;
        this.text = text;
        this.upVotes = upVotes;
        this.replies = new ArrayList<String>();
    }

    public ForumPost(String author, String text) {
        this.author = author;
        this.text = text;
         this.replies = new ArrayList<String>();
    }

    public String format() {
        String format = "";

        for (int i = 0; i < replies.size(); i++) {
            format += String.format("- %s%n", replies.get(i));
        }
        if(!replies.isEmpty()){
            format += String.format("%n");
        }
        return String.format("%s / by %s, %d votes. %n", this.text, this.author, this.upVotes) + format;
    }
}
