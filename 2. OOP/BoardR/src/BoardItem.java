import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class BoardItem {
    public static final int TITLE_MAX_LENGTH = 30;
    public static final int TITLE_MIN_LENGTH = 5;
    public final Status[] state = Status.values();
    public String title;
    private LocalDate dueDate;
    private Status status;
    private List<EventLog> items;

    public BoardItem(String title, LocalDate dueDate) {
        setTitle(title);
        setDueDate(dueDate);
        setStatus(Status.OPEN);
        this.items = new ArrayList<>();
        }

    public void addItem(EventLog item){
        this.items.add(item);
    }

    public List<EventLog> getItems() {
        return new ArrayList<>(items);
    }

    private void setStatus(Status status){
        this.status = status;
    }

    public Status getStatus(){
        return status;
    }

    public void setTitle(String title) {
        if (title == null) {
            throw new IllegalArgumentException("Please provide a non-empty title");
        }
        if (title.length() > TITLE_MAX_LENGTH || title.length() < TITLE_MIN_LENGTH) {
            throw new IllegalArgumentException("Please provide a title with length between 5 and 30 chars");
        }
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDueDate(LocalDate dueDate) {
        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("The date should never be in the past.");
        }
        this.dueDate = dueDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void advanceStatus() {
        if(this.status==Status.VERIFIED){
            return;
        }
        this.status = state[(this.status.ordinal() + 1) % state.length]; // Open = 0  (0 +1 ) % 5 = 1> Todo
    }

    public void revertStatus() {
        if(this.status==Status.OPEN){
            return;
        }
        this.status = state[(this.status.ordinal() - 1 + state.length) % state.length]; // Verified = 4 (4 - 1 + 5) % 5 = 3 DONE
    }

    public String viewInfo(){
        return String.format("'%s', [%s | %s]", title, this.status, dueDate);
    }

}
