import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class EventLog {
    private final String description;
    private final LocalDateTime timestamp = LocalDateTime.now();

    public EventLog(String description) {
        if (description == null) {
            throw new IllegalArgumentException("Description cannot be null");
        }
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getTimeStamp() {
        return timestamp;
    }

    public String viewInfo() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MMMM-yyyy hh:mm:ss");
        String dateFormat = getTimeStamp().format(dtf);
        return String.format("[%s] %s", dateFormat, getDescription());
    }
}
