package com.telerikacademy.oop.cosmetics.models.cart;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private List<Product> productList;

    public ShoppingCart() {
        productList = new ArrayList<>();
    }

    public List<Product> getProductList() {
        return new ArrayList<>(productList);
    }

    public void addProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException("Not implemented yet.");
        }
        productList.add(product);
    }

    public void removeProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException("Not implemented yet.");
        }
        productList.remove(product);
    }

    public boolean containsProduct(Product product) {
        boolean isFound = false;
        if (product == null) {
            throw new IllegalArgumentException("Not implemented yet.");
        }
        if ((getProductList().contains(product))) {
            isFound = true;
        }
        return isFound;
    }

    public double totalPrice() {
        double sum = 0;
        for (Product product:productList) {
            sum += product.getPrice();
        }
        return sum;
    }

}
