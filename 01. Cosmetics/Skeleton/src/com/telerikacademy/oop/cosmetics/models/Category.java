package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private static final int MIN_CATEGORY_NAME_LENGTH = 2;
    private static final int MAX_CATEGORY_NAME_LENGTH = 15;
    private String name;
    private List<Product> products;

    public Category(String name) {
        setName(name);
        products = new ArrayList<>();
    }

    private void setName(String name) {
        if (name.length() < MIN_CATEGORY_NAME_LENGTH || name.length() > MAX_CATEGORY_NAME_LENGTH) {
            throw new IllegalArgumentException("Category name should be between 2 and 15 symbols.");
        }
        this.name = name;
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }



    public void addProduct(Product product) {
        if(product == null){
            throw  new IllegalArgumentException("Not implemented yet.");
        }
        products.add(product);
    }


    public void removeProduct(Product product) {
        if(product==null){
            throw new IllegalArgumentException("Not implemented yet.");
        }
        if(!products.contains(product)){
            throw new IllegalArgumentException("Product cannot be found.");
        }
        products.remove(product);
    }

    public String print() {
        StringBuilder productPrint = new StringBuilder(String.format("#Category: [%s]%n",this.name));
        for (Product product:products) {
            productPrint.append(product.print());
        }
        return productPrint.toString();
    }

}
