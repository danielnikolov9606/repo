package com.telerikacademy.oop.cosmetics;

import com.telerikacademy.oop.cosmetics.core.EngineImpl;

import java.io.ByteArrayInputStream;

public class Startup {


    public static void userInput() {
        String input = "SomeCommand\n" +
                "CreateCategory S\n" +
                "CreateCategory Shampoos\n" +
                "CreateCategory Shampoos\n" +
                "CreateProduct MyMan 10.99 Men\n" +
                "CreateProduct MyMan N 10.99 Men\n" +
                "CreateProduct MyMan Nivea price Men\n" +
                "CreateProduct MyMan Nivea -5.99 Men\n" +
                "CreateProduct MyMan Nivea 10.99 Gender\n" +
                "CreateProduct MyMan Nivea 10.99 Men\n" +
                "CreateProduct MyMan Nivea 10.99 Men\n" +
                "AddProductToCategory Shampo MyMan\n" +
                "AddProductToCategory Shampoos MyBoy\n" +
                "AddProductToCategory Shampoos MyMan\n" +
                "CreateProduct MyWoman Nivea 17.99 Women\n" +
                "AddProductToCategory Shampoos MyWoman\n" +
                "ShowCategory Shampo\n" +
                "ShowCategory Shampoos\n" +
                "Exit\n";
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
    }

    public static void main(String[] args) {
        userInput();
        EngineImpl engine = new EngineImpl();
        engine.start();


    }

}