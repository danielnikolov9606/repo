package com.telerikacademy.oop.cosmetics.core;

import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;

public class ProductFactoryImpl implements ProductFactory {
    @Override
    public Category createCategory(String name) {
        return new CategoryImpl(name);
    }

    @Override
    public Product createProduct(String name, String brand, double price, GenderType gender) {
        return new ProductImpl(name, brand, price, gender);
    }
}
