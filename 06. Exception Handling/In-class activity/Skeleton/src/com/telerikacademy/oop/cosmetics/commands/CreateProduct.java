package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.Validator;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.IllegalParameterCountException;
import com.telerikacademy.oop.cosmetics.models.GenderType;

import java.util.List;

public class CreateProduct implements Command {
    private static final String PRODUCT_CREATED = "Product with name %s was created!";
    private static final int PARAMETERS_COUNT = 4;

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateProduct(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    @Override
    public void execute(List<String> parameters) {
        if(parameters.size() != PARAMETERS_COUNT){
            throw new IllegalParameterCountException("CreateProduct command expects 4 parameters.");
        }
        String name = parameters.get(0);
        String brand = parameters.get(1);
        double price = 0;
        try {
           price = Double.parseDouble(parameters.get(2));
        } catch (IllegalArgumentException e) {
            throw new NumberFormatException("Third parameter should be price (real number).");
        }
        String enumInput = parameters.get(3);
        Validator.validateEnums(enumInput, GenderType.values(), "Forth parameter should be one of Men, Women or Unisex.");
        GenderType gender = GenderType.valueOf(enumInput.toUpperCase());

        result = createProduct(name, brand, price, gender);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createProduct(String name, String brand, double price, GenderType gender) {
        if(productRepository.getProducts().containsKey(name)){
            throw new IllegalArgumentException(String.format("Product %s already exist.",name));
        }

        Product product = productFactory.createProduct(name, brand, price, gender);
        productRepository.getProducts().put(name, product);

        return String.format(PRODUCT_CREATED, name);
    }
}
