package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.Validator;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;

public class ProductImpl implements Product {
    private static final int BRAND_MIN_LENGTH = 2;
    private static final int BRAND_MAX_LENGTH = 10;
    private static final int NAME_MIN_LENGTH = 3;
    private static final int NAME_MAX_LENGTH = 10;
    private String name;
    private String brand;
    private double price;
    private final GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        Validator.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, "Product name");
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        Validator.validateStringLength(brand, BRAND_MIN_LENGTH, BRAND_MAX_LENGTH, "Product brand");
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        Validator.validateNegative(price);
        this.price = price;
    }

    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format(
                "#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n",
                name,
                brand,
                price,
                gender);
    }
}
