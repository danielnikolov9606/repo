package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.exceptions.IllegalParameterCountException;

import java.util.List;

public class CreateCategory implements Command {
    private static final String CATEGORY_CREATED = "Category with name %s was created!";
    private static final int PARAMETERS_COUNT = 1;

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateCategory(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() != PARAMETERS_COUNT) {
            throw new IllegalParameterCountException("CreateCategory command expects 1 parameter.");
        }
        String categoryName = parameters.get(0);
        result = createCategory(categoryName);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createCategory(String categoryName) {
        if (productRepository.getCategories().containsKey(categoryName)) {
            throw new IllegalArgumentException(String.format("Category %s already exist.", categoryName));
        }

        Category category = productFactory.createCategory(categoryName);
        productRepository.getCategories().put(categoryName, category);
        return String.format(CATEGORY_CREATED, categoryName);
    }
}
