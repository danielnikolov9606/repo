package com.telerikacademy.oop.cosmetics;

import com.telerikacademy.oop.cosmetics.commands.CommandType;

public class Validator {

    public static <T extends Enum<T>> void validateEnums(String input, Enum<T>[] enumArray, String message) {
        input = input.toUpperCase();
        boolean enumExists = false;
        for (Enum<T> anEnum : enumArray) {
            if (anEnum.toString().toUpperCase().equals(input)) {
                enumExists = true;
            }
        }
        if (!enumExists) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateNegative(double input) {
        if (input < 0) {
            throw new IllegalArgumentException("Price can't be negative.");
        }
    }

    public static void validateStringLength(String input, int minLength, int maxLength, String message) {
        if (input.trim().length() < minLength || input.trim().length() > maxLength){
            throw new IllegalArgumentException(String.format("%s should be between %d and %d symbols.", message, minLength, maxLength));
        }
    }
}
