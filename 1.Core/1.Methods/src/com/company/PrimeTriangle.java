package com.company;

import java.util.Scanner;

public class PrimeTriangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input = Integer.parseInt(scanner.nextLine());

        boolean[] primes = new boolean[input+1];
        for (int current = 1; current <= input; current++) {
            primes[current] = isPrime(current);
        }

        for (int current = 1; current <= input; current++) {
            if (!primes[current]) {
                continue;
            }

            for (int j = 1; j <= current; j++) {
                if (primes[j]) {
                    System.out.print(1);
                } else {
                    System.out.print(0);
                }
            }
            System.out.println();
        }
    }

    public static boolean isPrime(int number){
        if(number == 1){
            return true;
        }
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if(number % i == 0){
                return false;
            }
        }
        return true;
    }
}

