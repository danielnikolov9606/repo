package com.telerikacademy.cosmetics.models.validators;

import java.util.List;

public class ValidationHelper {

    public static void checkNull(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Input cannot be null.");
        }
    }

    public static void checkLength(String name, int minLength, int maxLength) {
        if (name.trim().length() <= minLength || name.trim().length() >= maxLength) {
            throw new IllegalArgumentException(String.format("Input length should be between %d and %d characters.", minLength, maxLength));
        }
    }

    public static void checkNegative(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative.");
        }
    }

    public static void checkNegative(int volume) {
        if (volume < 0) {
            throw new IllegalArgumentException("Price cannot be negative.");
        }
    }

    public static void validateList(List<String> list){
        if (list == null) {
            throw new IllegalArgumentException("List cannot be null.");
        }
    }

}
