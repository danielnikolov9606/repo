package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;

public class CreamImpl extends ProductImpl implements Cream {
    private static final int MIN_NAME_LENGTH = 3;
    private static final int MAX_NAME_LENGTH = 15;
    private static final int MIN_BRAND_LENGTH = 3;
    private static final int MAX_BRAND_LENGTH = 15;
    private ScentType scent;
    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        setScent(scent);
    }

    @Override
    public ScentType getScent() {
        return scent;
    }

    public void setScent(ScentType scent) {
        this.scent = scent;
    }

    @Override
    protected int getMinName() {
        return MIN_NAME_LENGTH;
    }

    @Override
    protected int getMaxName() {
        return MAX_NAME_LENGTH;
    }

    @Override
    protected int getMinBrand() {
        return MIN_BRAND_LENGTH;
    }

    @Override
    protected int getMaxBrand() {
        return MAX_BRAND_LENGTH;
    }

    @Override
    public String print() {
        return String.format("%s%n" +
                "#Scent: %s%n", super.print(), getScent());
    }
}
