package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.validators.ValidationHelper;

public abstract class ProductImpl implements Product {
    private String name;
    private String brand;
    private double price;
    private GenderType gender;


    public ProductImpl(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }


    public void setName(String name) {
        ValidationHelper.checkNull(name);
        ValidationHelper.checkLength(name, getMinName(), getMaxName());
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setBrand(String brand) {
        ValidationHelper.checkNull(brand);
        ValidationHelper.checkLength(brand, getMinBrand(), getMaxBrand());
        this.brand = brand;
    }

    public void setPrice(double price) {
        ValidationHelper.checkNegative(price);
        this.price = price;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        String text = String.format("#%s %s%n" +
                " #Price: $%.2f%n" +
                " #Gender: %s", getName(), getBrand(), getPrice(), getGender());
        return text;
    }
    protected abstract int getMinName();
    protected abstract int getMaxName();
    protected abstract int getMinBrand();
    protected abstract int getMaxBrand();
}
