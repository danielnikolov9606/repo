package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;
import com.telerikacademy.cosmetics.models.validators.ValidationHelper;

public class ShampooImpl extends ProductImpl implements Shampoo {
    private static final int MIN_NAME_LENGTH = 3;
    public static final int MAX_NAME_LENGTH = 10;
    public static final int MIN_BRAND_LENGTH = 2;
    public static final int MAX_BRAND_LENGTH = 10;
    private int milliliters;
    private UsageType usage;


    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {
        super(name, brand, price, gender);
        setMilliliters(milliliters);
        setUsage(usage);
    }
    @Override
    public int getMilliliters() {
        return milliliters;
    }

    private void setMilliliters(int milliliters) {
        ValidationHelper.checkNegative(milliliters);
        this.milliliters = milliliters;
    }
    @Override
    public UsageType getUsage() {
        return usage;
    }

    private void setUsage(UsageType usage) {
        ValidationHelper.checkNull(usage);
        this.usage = usage;
    }

    @Override
    protected int getMinName() {
        return MIN_NAME_LENGTH;
    }

    @Override
    protected int getMaxName() {
        return MAX_NAME_LENGTH;
    }

    @Override
    protected int getMinBrand() {
        return MIN_BRAND_LENGTH;
    }

    @Override
    protected int getMaxBrand() {
        return MAX_BRAND_LENGTH;
    }

    @Override
    public String print() {
        return String.format("#%s %s%n" +
                " #Price: $%.2f%n" +
                " #Gender: %s%n" +
                " #Milliliters: %d%n" +
                " #Usage: %s%n", getName(), getBrand(), getPrice(), getGender(),getMilliliters(),getUsage());
    }
}