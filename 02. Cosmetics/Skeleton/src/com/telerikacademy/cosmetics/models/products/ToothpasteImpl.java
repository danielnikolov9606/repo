package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;
import com.telerikacademy.cosmetics.models.validators.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductImpl implements Toothpaste {
    private static final int MIN_NAME_LENGTH = 3;
    private static final int MAX_NAME_LENGTH = 10;
    private static final int MIN_BRAND_LENGTH = 2;
    private static final int MAX_BRAND_LENGTH = 10;

    private List<String> ingredients;

    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);
        setIngredients(ingredients);
    }

    private void setIngredients(List<String> ingredients) {
        ValidationHelper.validateList(ingredients);
        this.ingredients = ingredients;
    }

    @Override
    protected int getMinName() {
        return MIN_NAME_LENGTH;
    }

    @Override
    protected int getMaxName() {
        return MAX_NAME_LENGTH;
    }

    @Override
    protected int getMinBrand() {
        return MIN_BRAND_LENGTH;
    }

    @Override
    protected int getMaxBrand() {
        return MAX_BRAND_LENGTH;
    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<String>(ingredients);
    }

    @Override
    public String print() {
        return String.format("#%s %s%n" +
                " #Price: $%.2f%n" +
                " #Gender: %s%n" +
                "#Ingridients: %s%n", getName(), getBrand(), getPrice(), getGender(),getIngredients());
    }
}
