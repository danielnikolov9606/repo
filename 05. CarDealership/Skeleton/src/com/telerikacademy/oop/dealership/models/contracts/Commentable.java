package com.telerikacademy.oop.dealership.models.contracts;

import java.util.List;

public interface Commentable {
    
    List<Comment> getComments();

    void addComment(Comment commentToAdd);

    void removeComment(Comment commentToRemove);
    
}
