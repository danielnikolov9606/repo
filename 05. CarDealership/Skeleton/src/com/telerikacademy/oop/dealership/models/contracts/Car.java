package com.telerikacademy.oop.dealership.models.contracts;

import com.telerikacademy.oop.dealership.models.common.enums.VehicleType;

import java.util.List;

public interface Car extends Vehicle {
    
    int getSeats();

    @Override
    int getWheels();

    @Override
    VehicleType getType();

    @Override
    String getMake();

    @Override
    String getModel();

    @Override
    List<Comment> getComments();

    @Override
    double getPrice();
}
