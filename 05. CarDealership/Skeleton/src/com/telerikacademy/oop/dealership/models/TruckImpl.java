package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.common.Constants;
import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.common.enums.VehicleType;
import com.telerikacademy.oop.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleImpl implements Truck {
    private static final int TRUCK_WHEELS_COUNT = 8;
    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, TRUCK_WHEELS_COUNT, price, VehicleType.TRUCK);
        setWeightCapacity(weightCapacity);
    }


    public void setWeightCapacity(int weightCapacity) {
        Validator.validateIntRange(weightCapacity, Constants.MIN_WEIGHT_CAPACITY, Constants.MAX_WEIGHT_CAPACITY, "Weight capacity must be between 1 and 100!");
        this.weightCapacity = weightCapacity;
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }
}
