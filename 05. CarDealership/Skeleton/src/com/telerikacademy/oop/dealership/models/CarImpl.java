package com.telerikacademy.oop.dealership.models;


import com.telerikacademy.oop.dealership.models.common.Constants;
import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.common.enums.VehicleType;
import com.telerikacademy.oop.dealership.models.contracts.Car;

public class CarImpl extends VehicleImpl implements Car {

    private static final int CAR_WHEELS_COUNT = 4;
    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, CAR_WHEELS_COUNT, price, VehicleType.CAR);
        setSeats(seats);
    }

    @Override
    public int getSeats() {
        return seats;
    }


    public void setSeats(int seats) {
        Validator.validateIntRange(seats, Constants.SEATS_MIN, Constants.SEATS_MAX, "Seats must be between 1 and 10!");
        this.seats = seats;
    }
}
