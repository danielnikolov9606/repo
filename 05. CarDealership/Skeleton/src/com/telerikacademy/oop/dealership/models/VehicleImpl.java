package com.telerikacademy.oop.dealership.models;


import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.common.enums.VehicleType;
import com.telerikacademy.oop.dealership.models.contracts.Comment;
import com.telerikacademy.oop.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.dealership.models.common.Constants.*;

public class VehicleImpl implements Vehicle {
    private static final double MIN_PRICE = 0.0;
    private static final double MAX_PRICE = 1000000.0;
    private String make;
    private String model;
    private int wheels;
    private double price;
    private VehicleType type;
    private final List<Comment> comments;

    protected VehicleImpl(String make, String model, int wheels, double price, VehicleType type) {
        setMake(make);
        setModel(model);
        setWheels(wheels);
        setPrice(price);
        setType(type);
        this.comments = new ArrayList<>();
    }

    private void setMake(String make) {
        Validator.validateArgumentIsNotNull(make, "Make cannot be null.");
        Validator.validateIntRange(make.trim().length(), MAKE_NAME_LEN_MIN, MAKE_NAME_LEN_MAX, "Make must be between 2 and 15 characters long!");
        this.make = make;
    }

    private void setModel(String model) {
        Validator.validateArgumentIsNotNull(model, "Model cannot be null.");
        Validator.validateIntRange(model.trim().length(), MODEL_NAME_LEN_MIN, MODEL_NAME_LEN_MAX,"Model must be between 1 and 15 characters long!");
        this.model = model;
    }

    private void setWheels(int wheels) {
        this.wheels = wheels;
    }


    private void setPrice(double price) {
        Validator.validateDecimalRange(price, MIN_PRICE, MAX_PRICE, "Price must be between 0.0 and 1000000.0!");
        this.price = price;
    }

    private void setType(VehicleType type) {
        this.type = type;
    }

    @Override
    public int getWheels() {
        return wheels;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    @Override
    public String getMake() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void addComment(Comment commentToAdd) {
        comments.add(commentToAdd);
    }

    @Override
    public void removeComment(Comment commentToRemove) {
        comments.remove(commentToRemove);
    }
}
