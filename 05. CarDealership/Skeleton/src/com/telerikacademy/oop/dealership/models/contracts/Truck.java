package com.telerikacademy.oop.dealership.models.contracts;

import com.telerikacademy.oop.dealership.models.common.enums.VehicleType;

import java.util.List;

public interface Truck extends Vehicle {
    
    int getWeightCapacity();

    @Override
    int getWheels();

    @Override
    VehicleType getType();

    @Override
    String getMake();

    @Override
    String getModel();

    @Override
    List<Comment> getComments();

    @Override
    double getPrice();
}
