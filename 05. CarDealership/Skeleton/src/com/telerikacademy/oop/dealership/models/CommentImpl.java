package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.common.Constants;
import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.contracts.Comment;

public class CommentImpl implements Comment {
    private String content;
    private String author;

    public CommentImpl(String content, String author) {
        setContent(content);
        setAuthor(author);
    }

    public void setAuthor(String author) {
        Validator.validateArgumentIsNotNull(author, "Author cannot be null.");
        this.author = author;
    }

    public void setContent(String content) {
        Validator.validateArgumentIsNotNull(content, "Content cannot be null");
        Validator.validateIntRange(content.trim().length(),
                Constants.CONTENT_LEN_MIN,
                Constants.CONTENT_LEN_MAX,
                "Content must be between 3 and 200 characters long.");
        this.content = content;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String getAuthor() {
        return author;
    }
}
