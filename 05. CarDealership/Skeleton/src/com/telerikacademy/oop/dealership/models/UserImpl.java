package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.common.Constants;
import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.common.enums.Role;
import com.telerikacademy.oop.dealership.models.contracts.Comment;
import com.telerikacademy.oop.dealership.models.contracts.User;
import com.telerikacademy.oop.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class UserImpl implements User {
    private int counter;
    private String username;
    private String firstName;
    private String lastName;
    private String password;
    private Role role;
    private List<Vehicle> vehicles = new ArrayList<>();

    public UserImpl(String username, String firstName, String lastName, String password, Role role) {
        setUserName(username);
        setFirstName(firstName);
        setLastName(lastName);
        setPassword(password);
        setRole(role);
//        this.vehicles = new ArrayList<>();
        counter = 0;
    }

    private void setUserName(String username) {
        Validator.validateArgumentIsNotNull(username, "UserName cannot be null.");

        Validator.validateIntRange(
                username.trim().length(),
                Constants.USERNAME_LEN_MIN,
                Constants.USERNAME_LEN_MAX,
                "Username must be between 2 and 20 characters long!");
        Validator.validatePattern(username, "^[A-Za-z0-9]+$", "Username contains invalid symbols!");
        this.username = username;
    }

    private void setFirstName(String firstName) {
        Validator.validateArgumentIsNotNull(firstName, "FirstName cannot be null");
        Validator.validateIntRange(
                firstName.trim().length(),
                Constants.FIRSTNAME_LEN_MIN,
                Constants.FIRSTNAME_LEN_MAX,
                "Firstname must be between 2 and 20 characters long!");
        this.firstName = firstName;
    }

    private void setLastName(String lastName) {
        Validator.validateArgumentIsNotNull(lastName, "LastName cannot be null.");
        Validator.validateIntRange(
                lastName.trim().length(),
                Constants.LASTNAME_LEN_MIN,
                Constants.LASTNAME_LEN_MAX,
                "Lastname must be between 2 and 20 characters long!");
        this.lastName = lastName;
    }

    private void setPassword(String password) {
        Validator.validateArgumentIsNotNull(password, "Password cannot be null.");

        Validator.validateIntRange(
                password.trim().length(),
                Constants.PASSWORD_LEN_MIN,
                Constants.PASSWORD_LEN_MAX,
                "Password must be between 5 and 30 characters long!");
        Validator.validatePattern(password, "^[A-Za-z0-9@*_-]+$", "Password contains invalid symbols.");
        this.password = password;
    }

    private void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Role getRole() {
        return role;
    }

    @Override
    public List<Vehicle> getVehicles() {
        return new ArrayList<>(vehicles);
    }

    @Override
    public void addVehicle(Vehicle vehicle) {
        if (role.equals(Role.ADMIN)) {
            throw new IllegalArgumentException("You are an admin and therefore cannot add vehicles!");
        }
        if (!role.equals(Role.VIP)) {
            if(counter <= 5) {
                vehicles.add(vehicle);
                counter++;
                return;
            } else {
                throw new IllegalArgumentException("You are not VIP and cannot add more than 5 vehicles!");
            }
        }
        vehicles.add(vehicle);
    }

    @Override
    public void removeVehicle(Vehicle vehicle) {
        vehicles.remove(vehicle);
    }

    @Override
    public void addComment(Comment commentToAdd, Vehicle vehicleToAddComment) {
        vehicleToAddComment.addComment(commentToAdd);
    }

    @Override
    public void removeComment(Comment commentToRemove, Vehicle vehicleToRemoveComment) {
        if (vehicleToRemoveComment.getComments().get(vehicleToRemoveComment.getComments().indexOf(commentToRemove)).getAuthor().equals(username)) {
            vehicleToRemoveComment.removeComment(commentToRemove);
        } else {
            throw new IllegalArgumentException("You are not the author of the comment you are trying to remove!");
        }
    }

    @Override
    public String printVehicles() {
        return null;
    }

}
