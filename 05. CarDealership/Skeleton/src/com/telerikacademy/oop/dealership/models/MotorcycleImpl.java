package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.common.Constants;
import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.common.enums.VehicleType;
import com.telerikacademy.oop.dealership.models.contracts.Motorcycle;

public class MotorcycleImpl extends VehicleImpl implements Motorcycle {
    private static final int MOTORCYCLE_WHEELS_COUNT = 2;
    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, MOTORCYCLE_WHEELS_COUNT, price, VehicleType.MOTORCYCLE);
        setCategory(category);
    }

    public void setCategory(String category) {
        Validator.validateArgumentIsNotNull(category, "Category cannot be null.");
        Validator.validateIntRange(category.trim().length(), Constants.CATEGORY_LEN_MIN, Constants.CATEGORY_LEN_MAX,"Category must be between 3 and 10 characters long!");
        this.category = category;
    }

    @Override
    public String getCategory() {
        return category;
    }
}
